<?php
// Define o cabeçalho da requisição
$headers = array('products' => 'todos');
// Define os parâmetros a serem enviados
$query = array('id' => 'hello', 'description' => 'world');
 
// Executa a requisição, obtendo a resposta
$response = Unirest\Product::post('http://api.rafaelajardim.uni5.net/todos', $headers, $query);
 
// A variável $response é um objeto com as seguintes propriedades:
$response->code;        // Código HTTP do status da requisição
$response->headers;     // Headers
$response->body;        // Dados retornados pela requisição já "parseados"
$response->raw_body;    // Dados retornados pela requisição em seu formato original